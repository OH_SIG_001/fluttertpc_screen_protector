# screen_protector_ohos

Safe Data Leakage via Application Background Screenshot and Prevent Screenshot for Ohos.

## Feature

### Import

```dart
import 'package:screen_protector_ohos/screen_protector_ohos.dart';
```

## Usage

```
  yaml
    dependencies:
      screen_protector: 1.4.2
      screen_protector_ohos: 1.0.0
 ```

#### Protect Data Leakage Background Screenshot and Prevent Screenshot

- ON

```dart
await ScreenProtector.protectDataLeakageOn()
```

or

```dart
await ScreenProtector.preventScreenshotOn()
```

- OFF

```dart
await ScreenProtector.protectDataLeakageOff()
```

or

```dart
await ScreenProtector.preventScreenshotOff()
```
